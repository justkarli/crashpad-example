// crashpad-example.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#define NOMINMAX

#include <client/crash_report_database.h>
#include <client/settings.h>
#include <client/crashpad_client.h>

#include <iostream>
#include <filesystem>
#include <map>
#include <Windows.h>
static bool startCrashHandler()
{
	using namespace crashpad;
	CrashpadClient client;

	auto cwd = std::filesystem::current_path();
	
	std::wstring db_path(cwd.wstring() + L"\\db");
	std::wstring handler_path(cwd.wstring() + L"\\bin\\crashpad_handler.exe");
	std::wstring cwd_path(cwd.wstring());

	base::FilePath db(db_path);
	base::FilePath handler(handler_path);
	base::FilePath binDir(cwd_path);

	std::string url;
	std::map<std::string, std::string> annotations;
	std::vector<std::string> arguments;

	std::unique_ptr<CrashReportDatabase> database = CrashReportDatabase::Initialize(db);

	if (database == nullptr || database->GetSettings() == NULL)
		return false;

	database->GetSettings()->SetUploadsEnabled(false);

	bool crashpadStarted = client.StartHandler(
		handler,
		db,
		binDir,
		url,
		annotations,
		arguments,
		true,
		true
	);

	if (!crashpadStarted)
		return false;

	// wait for crashpad to initialize

	crashpadStarted = client.WaitForHandlerStart(INFINITE);
	return crashpadStarted;
}

int main()
{
	if (startCrashHandler())
	{
		std::cout << "Hello World!\n";
		getchar();
		/*int* asdf = nullptr;
		std::cout << *asdf << std::endl;*/
			
		CONSOLE_FONT_INFO* lpfi = nullptr; 
		GetCurrentConsoleFont(GetStdHandle(STD_OUTPUT_HANDLE), false, lpfi);
	}

	getchar();
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
